# ================================================================
# ALIASES
# ================================================================

# Push and pop directories on directory stack
alias pu='pushd'
alias po='popd'

# Basic directory operations
alias ...='cd ../..'
alias -- -='cd -'

# Super user
alias _='sudo'
alias please='sudo'
alias oops='sudo !!'

# make files executable
alias ax='chmod a+x'

# alias g='grep -in'

# Show history
if [[ "$HIST_STAMPS" == "mm/dd/yyyy" ]]; then
  alias history='fc -fl 1'
elif [[ "$HIST_STAMPS" == "dd.mm.yyyy" ]]; then
  alias history='fc -El 1'
elif [[ "$HIST_STAMPS" == "yyyy-mm-dd" ]]; then
  alias history='fc -il 1'
else
  alias history='fc -l 1'
fi

# List direcory contents
alias ls='ls --color=auto'
alias la='ls -lAh --color=always | grep --color=never -v total'
alias lsa='ls -lAh'
alias l='ls -lah'
alias ll='ls -lh'
alias sl=ls           # often screw this up

alias return='cd "$OLDPWD"'

alias afind='ack-grep -il'

# bundle stuff
alias be='bundle exec'
alias bi='bundle install'
alias bu='bundle update'

# Other
alias C='clear'
alias home='cd ~'
alias path='echo $PATH'

alias passhelp="open -a /Applications/Firefox\ Nightly.app http://git.zx2c4.com/password-store/about/"
alias passtricks="open -a /Applications/Firefox\ Nightly.app https://gist.github.com/byronmansfield/7757507cde32df6240af"
alias gittricks="open -a /Applications/Firefox\ Nightly.app https://gist.github.com/byronmansfield/abcfb7641cd02b96d61d"
alias vimtricks="open -a /Applications/Firefox\ Nightly.app https://gist.github.com/byronmansfield/a33de96c0ec764db1e42"
alias tmuxtricks="open -a /Applications/Firefox\ Nightly.app https://gist.github.com/byronmansfield/f68e2ba0763cc410068b"
alias archtricks="open -a /Applications/Firefox\ Nightly.app https://gist.github.com/byronmansfield/5d1c34fca6834e0a3fe7"

# Hide/show all desktop icons (useful when presenting)
alias hidedesktop="defaults write com.apple.finder CreateDesktop -bool false && killall Finder"
alias showdesktop="defaults write com.apple.finder CreateDesktop -bool true && killall Finder"

## Zsh Stuff
alias eZ="vim ${HOME}/.zshrc"
alias eOMZ="vim ${HOME}/.oh-my-zsh/themes/msjche.zsh-theme"
alias Z="source ${HOME}/.zshrc"
alias eL="vim ${HOME}/.zlogin"
alias L="source ${HOME}/.zlogin"

## Vim Stuff
alias eV="vim ${HOME}/.vim/vimrc"
alias e="vim"
alias V="source ${HOME}/.vim/vimrc"

## Tmux
# alias tmux="tmux -2"
# alias tmux to use 256 color
alias tmux="TERM=screen-256color-bce tmux"
alias eT="vim ${HOME}/.tmux.conf"

## Aliases
alias eA="vim ${HOME}/.aliases"
alias A="source ${HOME}/.aliases"

## Functions
alias eF="vim ${HOME}/.functions"
alias F="source ${HOME}/.functions"

#########
## Git ##
#########

# The current branch name
# Usage example: git pull origin $(current_branch)
# Using '--quiet' with 'symbolic-ref' will not cause a fatal error (128) if
# it's not a symbolic ref, but in a Git repo.
current_branch() {
  local ref
  ref=$($_omz_git_git_cmd symbolic-ref --quiet HEAD 2> /dev/null)
  local ret=$?
  if [[ $ret != 0 ]]; then
    [[ $ret == 128 ]] && return  # no git repo.
    ref=$($_omz_git_git_cmd rev-parse --short HEAD 2> /dev/null) || return
  fi
  echo ${ref#refs/heads/}
}

alias g='git'
alias add='git add'
alias addall='git add --all'
alias commit='git commit -m'
alias branch='git branch'
alias rebase='git rebase'
alias gba='git branch -a'
alias gitremote='git branch --remote'
alias gbr='git branch --remote'
alias gco='git checkout'
alias gd='git diff'
alias fetch='git fetch'
alias setup='git branch --set-upstream-to=origin/$(current_branch)'
alias pull='git pull'
alias push='git push origin $(current_branch)'
alias fnp='git fetch && git pull'
alias reset='git reset HEAD'
alias resethard='git reset --hard'
alias resethh='git reset HEAD --hard'
alias status='git status'
alias stash='git stash'
alias stashapply='git stash apply'
alias dropstash='git stash drop'
alias liststash='git stash list'
alias stashpop='git stash pop'
alias subupdate='git submodule update'
alias tag='git tag -a -m'
alias pushtags='git push --tags'
alias deletetag='git tag -d'
alias pushdeletedtag='push origin :refs/tags/'

#-------------------- [ Docker ] --------------------#

alias dkr='docker'
alias imgs='docker images'
alias images='docker images'
alias containers='docker ps -a'
alias cntrs='docker ps -a'
alias dps='docker ps'
alias dpsa='docker ps -a'
alias container='docker run -it ubuntu:14.04 /bin/bash'
alias ctnr='docker run -it ubuntu:14.04 /bin/bash'
alias cleanimages='docker rmi -f $(docker images | grep "^<none>" | awk "{print $3}")'
alias clnimgs='docker rmi -f $(docker images | grep "^<none>" | awk "{print $3}")'
alias rmimgs='docker rmi -f $(docker images | grep "^<none>" | awk "{print $3}")'
alias rmi='docker rmi -f $(docker images | grep "^<none>" | awk "{print $3}")'
alias drmi='docker rmi'
alias drmif='docker rmi -f'
alias drm='docker rm'
alias cleancontainers='docker rm $(docker ps -a | grep "days ago" | awk '"'"'{print $1}'"'"')'
alias clncntrs='docker rm $(docker ps -a | grep "days ago" | awk '"'"'{print $1}'"'"')'
alias rmcntrs='docker rm $(docker ps -a | grep "days ago" | awk '"'"'{print $1}'"'"')'
alias cadvisor='sudo docker run --volume=/:/rootfs:ro --volume=/var/run:/var/run:rw --volume=/sys:/sys:ro --volume=/var/lib/docker/:/var/lib/docker:ro --publish=8080:8080 --detach=true --name=cadvisor google/cadvisor:latest'
alias cadvsr='sudo docker run --volume=/:/rootfs:ro --volume=/var/run:/var/run:rw --volume=/sys:/sys:ro --volume=/var/lib/docker/:/var/lib/docker:ro --publish=8080:8080 --detach=true --name=cadvisor google/cadvisor:latest'

# browser containers
alias ffctnr='xhost +; docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/.mozilla-ctnr:/root/.mozilla -v $HOME/Downloads-ctnr:/root/Downloads -e DISPLAY=192.168.1.6:0 --device /dev/snd --shm-size 2g --dns 9.9.9.9 --dns 149.112.112.112 --privileged bmansfield/firefox:74'
# alias chromectnr='IP=$(ifconfig | grep '"'"'inet '"'"' | grep -Fv 127 | grep 192 | awk '"'"'{print $2}'"'"'); export DISPLAY=$IP; xhost +; xhost + $IP; docker run -i -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$IP:0 --privileged jess/chrome'
alias chrmctnr='xhost +; docker run -it --rm --net host --cpuset-cpus 0 --memory 2GB -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=192.168.1.2:0 --privileged jess/chrome'
alias torctnr='IP=$(ifconfig | grep '"'"'inet '"'"' | grep -Fv 127 | grep 192 | awk '"'"'{print $2}'"'"'); export DISPLAY=$IP; xhost +; xhost + $IP; docker run -i -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$IP:0 --privileged jess/tor-browser'

# other containers
alias dvpn='docker run -d --rm --cap-add=NET_ADMIN -p 8112:8112 -p 8118:8118 -p 58846:58846 -p 58946:58946 -v ~/Downloads:/data -v ~/.config/deluge:/config -v ${DOCKERFILES}/arch-delugevpn/config/openvpn:/config/openvpn -e VPN_ENABLED=yes -e VPN_PROV=custom -e STRICT_PORT_FORWARD=yes -e ENABLE_PRIVOXY=no -e LAN_NETWORK=192.168.1.0/24 -e NAME_SERVERS=209.222.18.222,84.200.69.80,37.235.1.174,1.1.1.1,209.222.18.218,37.235.1.177,84.200.70.40,1.0.0.1 -e DEBUG=true -e UMASK=000 -e PUID=501 -e PGID=20 --name deluge binhex/arch-delugevpn'
alias signalctnr='IP=$(ifconfig | grep '"'"'inet '"'"' | grep -Fv 127 | grep 192 | awk '"'"'{print $2}'"'"'); export DISPLAY=$IP; xhost +; xhost + $IP; docker run -it --volume="$HOME/Downloads:/root/Downloads:rw" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -e DISPLAY=$IP:0 --name signal bmansfield/signal-desktop:latest'

alias stopdeluge='docker stop deluge'
alias rmdeluge='docker rm deluge'
alias delugedone='docker stop deluge && docker rm deluge'

# docker-compose
alias dc='docker-compose'
alias dcu='docker-compose up'
alias dcud='docker-compose up -d'
alias dcd='docker-compose down'

#-------------------- [ K8S Stuff ] --------------------#

# minikube
alias mk='minikube'
alias mkstart='minikube start'
alias mknew='minikube start --cpus 4 --memory 8196 --disk-size 50g --vm-driver="virtualbox"'
alias mkstop='minikube stop'
alias mkstatus='minikube status'
alias mkssh='minikube ssh'
alias mkdlt='minikube delete'
alias mkdash='minikube dashboard start'

# k8s basics
alias k='kubectl'
alias kns='kubens'
alias kct='kubectx'
alias kci='kubectl cluster-info'

# get
alias kg='kubectl get'
alias kga='kubectl get all'
alias kgaw='kubectl get all -o wide'
alias kgp='kubectl get po'
alias kgpw='kubectl get po -o wide'
alias kgsrv='kubectl get services'
alias kgsrvw='kubectl get services -o wide'
alias kgd='kubectl get deploy'
alias kgdw='kubectl get deploy -o wide'
alias kgn='kubectl get no'
alias kgv='kubectl get pvc,pv'
alias kgsct='kubectl get secrets'
alias kgsctw='kubectl get secrets -o wide'

# describe
alias kdc='kubectl describe'
alias kdcp='kubectl describe pods'
alias kdcsrv='kubectl describe service'
alias kdcsct='kubectl describe secrets'
alias kdcd='kubectl describe deployment'

# create
alias kc='kubectl create'
alias kdcsct='kubectl create secret'

# apply
alias ka='kubectl apply'
alias kaf='kubectl apply -f'

# delete
alias kdl='kubectl delete'

# logs
alias kl='kubectl logs'
alias klf='kubectl logs -f'

# other
alias ktop='kubectl top'
alias kpf='kubectl port-forward'
alias ka='kubectl attach'
alias kexec='kubectl exec -it'
alias kdiff='kubectl diff'
alias kcp='kubectl cp'

# helm
alias h='helm'
alias hc='helm create'
alias hs='helm status'
alias hd='helm delete'
alias hu='helm upgrade'
alias hls='helm list'
alias hga='helm list --all --all-namespaces'
alias hl='helm lint'

# cd's
alias up1='cd ..'
alias up2='cd ../..'
alias up3='cd ../../..'
alias personalcode='cd $CODESPACE/personal'
alias practicecode='cd $CODESPACE/practice'

# unzip and compress with tar
alias tgx='tar -zxvf'
alias tgc='tar -zcvf'
alias tbx='tar -jxvf'

# find files with no known users
alias findorphens='find / -nouser -ls'

# links stuff
alias lsusb='system_profiler SPUSBDataType'
alias lsblk='diskutil list'

# matrix for fun
alias matrix='unimatrix'

# todolist
alias t='/usr/local/bin/todolist'

# tree
alias tree='tree -a -C -I ".git|venv|__pycache__"'

# weather
alias weather='curl wttr.in/Redondo+Beach'
alias weather2='curl v2.wttr.in/Redondo+Beach'
