# ================================================================
#          _
#  _______| |__  _ __ ___
# |_  / __| '_ \| '__/ __|
#  / /\__ \ | | | | | (__
# /___|___/_| |_|_|  \___|
#
# ================================================================
#
#
# ================================================================
# System
# ================================================================
export ZSH=${HOME}/.oh-my-zsh

# locale settings
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# set display for x forwarding
export DISPLAY=:0.0

# term
export TERM="xterm-256color-italic"

# Some flags to help with compilation from source
export CPPFLAGS="-I/usr/local/openssl/include"
export LDFLAGS="-L/usr/local/openssl/lib"
export USE_SYSTEM_LIBCLANG=ON
export PKG_CONFIG_PATH="/usr/X11/lib/pkgconfig"
export MANPATH="/usr/local/man:/usr/share/man:${MANPATH}"

# ================================================================
# OMGZSH specific
# ================================================================
ZSH_THEME="byron"
DISABLE_AUTO_TITLE="true"

plugins=(git docker pass)

source $ZSH/oh-my-zsh.sh

# ================================================================
# USER CONFIGURATIONS
# ================================================================

# Set vim as default editor
export VISUAL=vim
export EDITOR=${VISUAL}

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/X11/bin"

[[ -d "${HOME}"/bin ]] && export PATH="$PATH:$HOME/bin"

[[ -f "${HOME}"/.aliases ]] && source "${HOME}"/.aliases
[[ -f "${HOME}"/.functions ]] && source "${HOME}"/.functions

[[ -f "${HOME}"/.aws/env ]] && source "${HOME}"/.aws/env
# [[ -f "${HOME}"/.work ]] && source ${HOME}/.work

export CODESPACE=${HOME}/code
export DOCKERFILES=${CODESPACE}/dockerfiles

# ssh
export SSH_KEY_PATH="~/.ssh/id_rsa"
eval $(keychain --nogui --eval --agents ssh --inherit any id_rsa)

# GnuPG
export GPGKEY="217FD2E8"
export GPG_AGENT_INFO
export GPG_TTY=$(tty)

# Python
export PATH="$PATH:/Library/Frameworks/Python.framework/Versions/3.7/bin"
# export PATH="$PATH:$HOME/Library/Python/2.7/bin"

# auto complete for pip
compctl -K _pip_completion pip

# Rust
export PATH="$PATH:$HOME/.cargo/bin"

# Go
export GOARCH="amd64"
export GOOS="darwin"
export GOROOT=/usr/local/go
export PATH="$PATH:$GOROOT/bin"
export GOPATH=$HOME/go
export PATH="$PATH:$GOPATH/bin"

# gcloud
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/usr/local/bin/gcloud/path.zsh.inc' ]; then
  . '/usr/local/bin/gcloud/path.zsh.inc'
fi

# Enables shell command completion for gcloud
if [ -f '/usr/local/bin/gcloud/completion.zsh.inc' ]; then
  source '/usr/local/bin/gcloud/completion.zsh.inc';
fi

# Archy for fun
archey
